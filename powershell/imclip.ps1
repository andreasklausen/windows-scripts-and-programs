$name=$Args[0]
$user=$env:userprofile

$fileexistspng=Test-Path -Path ./$name".png"
$fileexistsjpg=Test-Path -Path ./$name".jpg"

if ($fileexistspng) {
    "Choose another filename"
}
elseif($fileexistsjpg) {
    "Choose another filename"
}
else {
    python $user\git\scripts\python\save-image-clipboard.py $name
    optimize-images $name".png"
}