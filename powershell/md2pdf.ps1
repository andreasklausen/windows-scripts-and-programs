# Put input without square brackets
$arg = $Args[0]

# Get name with extension
# https://stackoverflow.com/questions/12503871/removing-path-and-extension-from-filename-in-powershell
$name = [io.path]::GetFileName("$arg")
$namepdf = [io.path]::GetFileNameWithoutExtension("$arg") + ".pdf"

# Run pandoc
pandoc -o $namepdf $name

# Show PDF in Sumatra
$userprof = $Env:USERPROFILE
& $userprof\AppData\Local\SumatraPDF\SumatraPDF.exe $namepdf