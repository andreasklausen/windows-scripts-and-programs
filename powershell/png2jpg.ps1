# Convert a PNG image to a JPG image
# Usage: makegif.ps1 ./some-name.png
# Example: makegif test.png

# Put input without square brackets
$arg = $Args[0]

# Get name without extension
# https://stackoverflow.com/questions/12503871/removing-path-and-extension-from-filename-in-powershell
$name = [io.path]::GetFileNameWithoutExtension("$arg")

# Convert PNG to JPG
magick convert $arg "$name.jpg"