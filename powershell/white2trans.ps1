# Convert a PNG image to a JPG image, and optionally reduce its size.
# Usage: makegif.ps1 ./some-name.png [newheight in pixels]
# Example: makegif test.png 720

# Put input without square brackets
$arg = $Args[0]

# Get name without extension
# https://stackoverflow.com/questions/12503871/removing-path-and-extension-from-filename-in-powershell
$name = [io.path]::GetFileNameWithoutExtension("$arg")
$extension=[System.IO.Path]::GetExtension($arg)

if ($extension -eq ".jpg") {
    magick.exe convert "${name}.jpg" "$name.png"
    magick.exe convert "${name}.png" -transparent white "${name}-trans.png"
    Remove-Item "${name}.png"
}
Else {
    magick.exe convert "${name}.png" -transparent white "${name}-trans.png"
}
