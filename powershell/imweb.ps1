# Save images from web and also save the link as a txt file
# Makes both optimized .jpg files and .png file with transparency instead of white

$page=Get-Clipboard
$name=$Args[0]

$extension=[System.IO.Path]::GetExtension($page)

$fileexistspng=Test-Path -Path ./$name".png"
$fileexistsjpg=Test-Path -Path ./$name".jpg"

if ($fileexistspng) {
    "Choose another filename"
}
elseif($fileexistsjpg) {
    "Choose another filename"
}
else {
    wget -OutFile $name$extension $page
    optimize-images.exe "${name}${extension}" -mh 1080
    $page > $name".txt"
}
