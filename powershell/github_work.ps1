git config --global user.name $env:my_name
git config --global user.email $env:my_work_email
git config --global alias.up "!git remote update -p; git merge --ff-only @{u}"
ssh-add -D
ssh-add %USERPROFILE%\.ssh\id_ed25519_work