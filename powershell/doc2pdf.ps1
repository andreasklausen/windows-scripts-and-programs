# Get the path of running script
$mypath = $MyInvocation.MyCommand.Path

# Go back three folders to reach the parent folder "scripts"
$mypath = Split-Path $mypath -Parent
$mypath = Split-Path $mypath -Parent
$mypath = Split-Path $mypath -Parent

# Run python
$arg0 = $Args[0]
python "$mypath\python\doc2pdf.py" "$arg0"
read-host "Finished converting. Press ENTER to exit..."