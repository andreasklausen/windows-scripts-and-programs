# Based on https://tex.stackexchange.com/questions/240243/getting-gif-and-or-moving-images-into-a-latex-presentation
# Convert a gif into multiple png images in a new folder.
# Usage: makegif.ps1 ./some-name.gif
# OR makegif.ps1 some-name
# OR makegif.ps1 some-name.gif

# Put input without square brackets
$arg = $Args[0]

# Get name without extension
# https://stackoverflow.com/questions/12503871/removing-path-and-extension-from-filename-in-powershell
$name = [io.path]::GetFileNameWithoutExtension("$arg")

# Make the directory
mkdir $name
cd $name

# Convert the gif to multiple PNGs
# https://tex.stackexchange.com/questions/240243/getting-gif-and-or-moving-images-into-a-latex-presentation
magick convert -coalesce ".\..\$name.gif" "$name.jpg"

# Optimize all the images with standard python env
optimize-images .

# Get the delay
# https://tex.stackexchange.com/questions/240243/getting-gif-and-or-moving-images-into-a-latex-presentation
"The next output gives delay. With 7x100, then the frame rate is 100 divided by 7"
magick identify -verbose ".\..\$name.gif" | sls -Pattern 'Delay' | sls -Pattern 'Delay'

# Say how many files should be put into latex command
# https://stackoverflow.com/questions/24374306/powershell-get-number-of-files-and-folders-in-directory
$count = (GCI|?{!$_.PSIsContainer}).Count - 1
"Put $count in latex"

# Change back
cd ..