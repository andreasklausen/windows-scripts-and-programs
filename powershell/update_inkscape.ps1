# Get the path of running script
$mypath = $MyInvocation.MyCommand.Path

# Go back three folders to reach the parent folder "scripts"
$mypath = Split-Path $mypath -Parent
$mypath = Split-Path $mypath -Parent
$mypath = Split-Path $mypath -Parent

# Run python
python "$mypath\python\update_inkscape.py"