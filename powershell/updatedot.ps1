$us=$Env:USERPROFILE

mv -Force $us\AppData\Local\nvim\init.vim $us\AppData\Local\nvim\init.vim.old
cp $us\git\scripts\windows\nvim\init.vim $us\AppData\Local\nvim\init.vim

mv -Force $us\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json $us\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json.old
cp $us\git\scripts\windows\terminal-settings\terminal_settings.json $us\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json

mv -Force $us\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1 $us\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1.old
cp $us\git\scripts\windows\powershell-profile\profile.ps1 $us\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
