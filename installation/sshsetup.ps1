#Requires -RunAsAdministrator

# Start SSH agent
Set-Service ssh-agent -StartupType Automatic
Start-Service ssh-agent
Get-Service ssh-agent

# Make key for this computer
mkdir $Env:USERPROFILE/.ssh
ssh-keygen -o -a 100 -t ed25519 -f $Env:USERPROFILE/.ssh/id_ed25519 -C $Env:COMPUTERNAME
ssh-keygen -o -a 100 -t ed25519 -f $Env:USERPROFILE/.ssh/id_ed25519_work -C $Env:COMPUTERNAME

# Make Git use SSH-agent in Windows
[Environment]::SetEnvironmentVariable("GIT_SSH", "$((Get-Command ssh).Source)", [System.EnvironmentVariableTarget]::User)

# Sources
# First make [openssh server start up automatically](https://code.visualstudio.com/docs/remote/troubleshooting#_setting-up-the-ssh-agent).
# To make [GIT for Windows use the OpenSSH service](https://snowdrift.tech/cli/ssh/git/tutorials/2019/01/31/using-ssh-agent-git-windows.html)
