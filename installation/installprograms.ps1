#Requires -RunAsAdministrator

# Install Chocolatey
# Set-ExecutionPolicy AllSigned
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# Install programs using Chocolatey
choco install --confirm 7zip
choco install --confirm foxitreader
choco install --confirm sumatrapdf
choco install --confirm jpegview
choco install --confirm treesizefree
choco install --confirm rclone

choco install --confirm everything
choco install --confirm grepwin
choco install --confirm autohotkey

choco install --confirm imagemagick

choco install --confirm pandoc
choco install --confirm hugo-extended
choco install --confirm strawberryperl

choco install --confirm notepadplusplus
choco install --confirm vscode
choco install --confirm vscode-python
choco install --confirm pdftk

choco install --confirm git
# choco install --confirm vcredist140
# choco install --confirm visualcppbuildtools
choco install --confirm cmake --installargs "ADD_CMAKE_TO_PATH=User"
choco install --confirm lf
