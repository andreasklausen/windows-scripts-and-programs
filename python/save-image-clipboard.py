'''
Save the image in clipboard to a file and pass with it the title of the window active when making the screenshot
Only works in Windows for now

usage 1:
python save-image-clipboard.py screen_shot
This will save the screenshot to the folder defined by the environemnt variable "screenshot_folder"

usag2 :
python save-image-clipboard.py imagename
This will save the screenshot to the current folder with the name "imagename"
'''

from PIL import ImageGrab
import sys
import os
import datetime
import shutil
import time
from pathlib import Path

def main():
    img = ImageGrab.grabclipboard()
    if len(sys.argv) < 2:
        print('Need to pass a filename')
        time.sleep(3)
        sys.exit()
    argument = sys.argv[1]
    path, filename = get_path(argument)
    fullpath_png = path.joinpath(filename + '.png')
    fullpath_txt = path.joinpath(filename + '.txt')
    img.save(fullpath_png)
    shutil.copy(Path(os.environ['USERPROFILE']).joinpath('imclipWindowTitle.txt'), fullpath_txt)
    return 0

def get_path(argument):
    if argument == 'screen_shot':
        # Use current time as filename
        if not 'screenshot_folder'.upper() in list(os.environ.keys()):
            print('screenshot_folder environment variable is not defined')
            time.sleep(3)
            sys.exit()
        else:
            path = Path(os.environ['screenshot_folder'])
            if not path.exists():
                os.makedirs(path)
        filename = datetime.datetime.now().replace(microsecond=0).isoformat()
        filename = filename.replace(':', '-').replace('T', ' ')
    else:
        # Use the given filename
        path = Path('.')
        filename = argument
    return path, filename

if __name__ == '__main__':
    main()
