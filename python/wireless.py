import subprocess
import time

while True:
    a = subprocess.run("ipconfig", stdout=subprocess.PIPE)
    b = a.stdout
    b = b.decode("UTF-8").replace('\r', '')

    i1 = b.find("Wireless LAN adapter Wi-Fi:")
    i2 = b[i1:].find('\n\n') + i1
    i3 = b[i2+2:].find('\n\n') + i2

    if 'disconnected' in b[i2:i3]:
        print(time.strftime("%c"))
        print("Reconnecting network...")
        subprocess.run("netsh wlan connect name=eduroam")
    else:
        pass
        # print("Already connected")

    time.sleep(10)