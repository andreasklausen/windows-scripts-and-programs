'''
Converts all the powerpoint and word files to PDF recirsively from the current working directory
'''

import os
import sys
from win32com import client
from glob import glob
import time

def _findFiles(path):
    '''
    Recursively finds all the word and powerpoint files starting at the current directory
    '''
    temp = glob(path + "*")
    fileNames = []
    if len(temp) > 0:
        for i in range(0, len(temp)):
            splitfile = temp[i].split(".")
            if (
                splitfile[-1] == "doc"
                or splitfile[-1] == "docx"
                or splitfile[-1] == "pptx"
                or splitfile[-1] == 'pptm'
            ):
                if not "~" in splitfile[0]:
                    fileNames.append(temp[i])

    folders = glob(path + "*")
    for i in range(0, len(folders)):
        temp = _findFiles(folders[i] + "\\")
        for i in range(0, len(temp)):
            splitfile = temp[i].split(".")
            if (
                splitfile[-1] == "doc"
                or splitfile[-1] == "docx"
                or splitfile[-1] == "pptx"
                or splitfile[-1] == 'pptm'
            ):
                if not "~" in splitfile[0]:
                    fileNames.append(temp[i])
    return fileNames

def doc2pdf(folder, deleteSource=False):
    wordClientStarted = False
    pptClientStarted = False
    filenames = _findFiles(folder)
    for filename in filenames:
        extension = filename.split(".")[-1]
        out_name = filename.replace(extension, r"pdf")
        in_file = os.path.abspath(filename)
        print('Processing ', in_file)
        out_file = os.path.abspath(out_name)
        if extension in ["doc", "docx"]:
            if wordClientStarted == False:
                word = client.DispatchEx("Word.Application")
                word.Visible = 1
                wordClientStarted = True
            doc = word.Documents.Open(in_file)
            time.sleep(1)
            doc.SaveAs(out_file, FileFormat=17)
            doc.Close()
            time.sleep(1)
        elif extension in ["pptx", 'pptm']:
            if pptClientStarted == False:
                powerpoint = client.DispatchEx("Powerpoint.Application")
                powerpoint.Visible = 1
                pptClientStarted = True
            doc = powerpoint.Presentations.Open(in_file)
            time.sleep(1)
            doc.SaveAs(out_file, FileFormat=32)
            doc.Close()
            time.sleep(1)
        if deleteSource:
            time.sleep(0.1)
            os.remove(in_file)

    if wordClientStarted:
        word.Close()
    if pptClientStarted:
        powerpoint.close()


if __name__ == '__main__':
    path = os.getcwd()
    deleteSource = False

    doc2pdf(folder=path, deleteSource=deleteSource)
