'''
This script gets the current IPv4 address of a certain network adapter every 5 minutes and saves to a file
Only works on Windows!
'''

import os
import subprocess
import time
import sys

iterations = 0

adapter = sys.argv[1].lower()
savelocation = sys.argv[2]

while True:
    a = subprocess.run("ipconfig", stdout=subprocess.PIPE)
    b = a.stdout
    b = b.decode("UTF-8").replace('\r', '').lower()

    i1 = b.find("{}:".format(adapter))
    i2 = b[i1:].find('\n\n') + i1
    if b[i2+2:].find('\n\n') == -1:
        i3 = len(b)
    else:
        i3 = b[i2+2:].find('\n\n') + i2
    c = b[i2:i3]
    i4 = c.find("ipv4 address")
    i5 = c[i4:].find('\n') + i4
    d = c[i4:i5]
    i6 = d.find(":")
    ipaddress = d[i6+2:]
    if iterations == 0:
        print('%s : Current IP address is %s' % (time.strftime("%c"), ipaddress))

    ipaddressFilename = savelocation
    if os.path.exists(ipaddressFilename):
        with open(ipaddressFilename, 'r') as outfile:
            oldipaddress = outfile.read()
        if not oldipaddress == ipaddress:
            with open(ipaddressFilename, 'w') as infile:
                infile.write(ipaddress)
                print('%s : IP address changed to %s' % (time.strftime("%c"), ipaddress))
    else:
        with open(ipaddressFilename, 'w') as infile:
            infile.write(ipaddress)
    iterations += 1
    time.sleep(300)
