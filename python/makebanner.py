'''
Make a banner which can be pasted in the code

Example:
makebanner.py main
'''

import pyfiglet
import pyperclip
import sys

text = sys.argv[1]
if len(sys.argv) >= 3:
	indent = int(sys.argv[2])
else:
	indent = 0

b = pyfiglet.figlet_format(text, font='banner3')
bs = b.split('\n')
comment = len(bs[0])*'#'
ret = indent*'\t' + comment + '\n\n'
for e in bs:
	ret += indent*'\t' + e + '\n'
ret += indent*'\t' + comment
pyperclip.copy(ret)
