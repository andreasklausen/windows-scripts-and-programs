'''
Resize all the images in the current folder to a desired width

Usage:
python resizeimages.py 1080
'''

import os
import sys
from glob import glob
from PIL import Image
from resizeimage import resizeimage
from pathlib import Path

def join(l):
    path = l[0] + '\\'
    for a in l[1:]:
        path = os.path.join(path, a)
    return path

def resize(folder, wantedwidth):
    print('Folder: %s' % (folder))
    explorer = Path(folder)
    filenames = [x for x in explorer.iterdir() if x.is_file()]
    if not os.path.exists(os.path.abspath(folder) + '\\resized'):
        os.mkdir(os.path.abspath(folder) + '\\resized')

    for filename in filenames:
        in_file = os.path.abspath(filename)
        print('Input: %s' % (in_file))
        out_file = join(in_file.split('\\')[:-1]) + '\\resized\\' + in_file.split('\\')[-1]
        print('Output: %s\n' % (out_file))

        with open(in_file, 'rb') as infile:
            img = Image.open(infile)
            wantedheight = int(round(wantedwidth*img.height/img.width))
            img = img.resize((wantedwidth, wantedheight), Image.ANTIALIAS)
            img.save(out_file, img.format)

def main():
    folder = os.getcwd()
    args = sys.argv
    if len(args) == 1:
        print('Add input on wanted image width, i.e. 1080')
    wantedwidth = args[1]
    resize(folder=folder, wantedwidth)

if __name__ == '__main__':
    main()
