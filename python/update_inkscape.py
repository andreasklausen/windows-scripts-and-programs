#!/usr/bin/python3
r"""
Update images from Inkscape by saving them to PDF once changed
This is useful for making Beamer or articles or whatever

Usage:
Be in a folder with the inkscape documents and run
python update_inkscape
Then, the PDFs will be stored in the parent folder

For example, you would have the folders project/images/inkscape.
"""

from pathlib import Path
import hashlib
import os
import pickle
import time
import shutil
import subprocess
import sys

def main():
    # Set paths to operate in
    path_inkscape = Path('.').absolute()
    path_pdfs = Path('./..').absolute()

    # Initializtion
    hashes_changed = False
    make_pdf = False
    any_pdfs_made = False

    # Load the hashes if available:
    if path_inkscape.joinpath("hashes").exists():
        with open(path_inkscape.joinpath("hashes"), "rb") as infile:
            hashes = pickle.load(infile)
    else:
        hashes = {}

    # Always stay alive
    while True:
        filepaths = list(path_inkscape.glob("*.svg"))
        for filepath in filepaths:
            # Read the hash
            with open(filepath, "rb") as outfile:
                allbytes = outfile.read()  # read entire file as bytes
                readable_hash = hashlib.sha256(allbytes).hexdigest()

            # Check if hash not exist, or if changed
            file_path_pdf = path_pdfs.joinpath(filepath.stem + ".pdf")
            if filepath.name not in hashes:
                hashes[filepath.name] = readable_hash
                hashes_changed = True
                make_pdf = True
            elif hashes[filepath.name] != readable_hash:
                hashes[filepath.name] = readable_hash
                hashes_changed = True
                make_pdf = True
            elif not file_path_pdf.exists():
                print(str(file_path_pdf))
                make_pdf = True

            # Make PFF if hash has changed
            if make_pdf:
                flag = os.popen(f"inkscape {str(filepath)} --export-area-page --batch-process --export-type=pdf --export-filename={str(file_path_pdf)}")
                make_pdf = False
                any_pdfs_made = True

        # If hashes changes, save them to file
        if hashes_changed:
            with open(path_inkscape.joinpath("hashes"), "wb") as outfile:
                pickle.dump(hashes, outfile)
            hashes_changed = False

        # Wait for some time if some PDFs are made, to give Inkscape some time to finish
        if any_pdfs_made:
            time.sleep(10)
        else:
            time.sleep(1)

if __name__ == '__main__':
    main()
