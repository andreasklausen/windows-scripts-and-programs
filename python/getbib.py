'''
Get the bibtex library from a DOI and puts that in the clipboard

Usage: python getbib.py 10.1016/j.biopha.2009.03.005
'''

import sys
from pathlib import Path

import bibtexparser
import pyperclip
from habanero import cn

def main():
    """Main function"""
    # Check if enough input arguments
    if len(sys.argv) < 2:
        print("Either input debug or a DOI")
        return

    # Run debug mode with pre-set citation
    if sys.argv[1] == "debug":
        print("Running debug session with pre-set citation")
        citation = """@article{Costa_2010,
        doi = {10.1016/j.biopha.2009.03.005},
        url = {https://doi.org/10.1016%2Fj.biopha.2009.03.005},
        year = 2010,
        month = {jan},
        publisher = {Elsevier {BV}},
        volume = {64},
        number = {1},
        pages = {21--28},
        author = {L.S. Costa and G.P. Fidelis and S.L. Cordeiro and R.M. Oliveira and D.A. Sabry and R.B.G. C{\^{a}}mara and L.T.D.B. Nobre and M.S.S.P. Costa and J. Almeida-Lima and E.H.C. Farias and E.L. Leite and H.A.O. Rocha},
        title = {Biological activities of sulfated polysaccharides from tropical seaweeds},
        journal = {Biomedicine {\&} Pharmacotherapy}
    }"""
    else:  # Or get citation
        # Get citation from crossref
        citation = cn.content_negotiation(ids=sys.argv[1], format='bibtex')

    # Parse the citation
    bib_database = bibtexparser.loads(citation)

    # Get title
    title = bib_database.entries[0]['title']
    old_id = bib_database.entries[0]['ID']

    # Rename citation ID based on current ID and title
    surname = old_id.split('_')[0].lower()
    year = old_id.split('_')[1]
    title_allchars = "".join([arg for arg in title.split(' ')[:3]]).lower()
    illegal_characters = ":-"
    for char in illegal_characters:
        title_allchars = title_allchars.replace(char, "")
    new_id = surname + year + title_allchars
    citation_new = citation.replace(old_id, new_id)

    # Copy the citation
    pyperclip.copy(citation_new)

    # Rename file if wanted to
    if len(sys.argv) == 3:
        path = Path(sys.argv[2])
        newpath = Path(path.parent).joinpath(new_id + ".pdf")
        path.rename(newpath)

if __name__ == "__main__":
    main()
