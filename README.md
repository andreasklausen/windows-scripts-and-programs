# windows-scripts
Scripts, hotkeys, programs and settings used in a Windows 10 environment.

# Summary of scripts and useful programs:
- Automatically saving screen shots
- Automatic PDF generation from Inkscape
- Make banners for code headers (Python)
- Saving IP address to a file
- Convert all office files to pdf (Python)
- Chocolatey for installing programs
- Everything for searching files quickly
- GrepWin to search for text in all files in a folder
- ScanIt to acquire data from lines in plots

# Setup
## Download this repository
Start by downloading this repository to a known location.

## Install AutoHotKey
AutoHotKey (ahk) is used to simulate keystrokes in Windows, and perform tasks through scripting.

Install ahk from [here](https://www.autohotkey.com/).

## Add environment variables and change PATH
For guidance, please see [System variables (How-To)](##system-variables-(how-to))

Add the following to User PATH (where `...` is the path to the `scripts` folder):
- `...\powershell`

Add the following User variable:
- `screenshot_folder` = `C:\Path\To\Save\Screenshots`

# Description of scripts
## Automatically saving screen snippets to a designated folder
I am a frequent user of WIN + SHIFT + S to take a partial screenshot.
However, it is a "pain" to open Paint, paste the image from clipboard and save the file to some location.
Instead, I have opted to use AutoHotKey to automate this process and save the screenshot to a designated location with the timestamp of taking the screen shot.

In order to use this, the following has to be done:

Add `...\autohotkey-scripts\ahkfunctions.ahk` to the Windows startup folder, so it runs every time Windows starts up
1. Open a file explorer window
2. Click the address bar, and type in `startup`
3. Once in the startup folder, right click the empty space and make `New --> Shortcut`
4. Click to `Browse` for the file, and choose the `.../autohotkey-scripts/ahkfunctions.ahk` file.
5. Start the shortcut so that ahk is running.

Now, screenshots can be made by holding down `CTRL` and `LEFT ALT`, and press `P`.
Once these buttons are released, the screenshot tool will open, and you can take a portion of the screen. The screen shot will be saved to your location of choice, with the current time as filename.
Additionally, a `.txt` file also shows which program was active, or the web-page visited when performing the screen shot.

## Automatically saving Inkscape drawings as PDF (Requires Python)
Instead of saving a copy of the Inkscape file as .pdf or .eps manually, I have made a script that checks for changes to .svg files, and then automatically saves a copy as pdf.
The general way of using this functionality is to have an image folder (for example `fig`), and make an Inkscape folder inside (`fig/inkscape`).
Opening a terminal in the `fig/inkscape` folder and typing `update_inkscape` will start the process.

## Make banners for code (Requires Python)
Use the script `makebanner` in a terminal with a string input.
For example, `makebanner main` will produce the following in your clipboard (just paste where you want it)
```
##################################

##     ##    ###    #### ##    ## 
###   ###   ## ##    ##  ###   ## 
#### ####  ##   ##   ##  ####  ## 
## ### ## ##     ##  ##  ## ## ## 
##     ## #########  ##  ##  #### 
##     ## ##     ##  ##  ##   ### 
##     ## ##     ## #### ##    ## 

##################################
```

## Automatically saving IP address to a file (Requires Python)
Useful for remote-desktop purposes if you have little control of the IP address in your network, and cannot resolve computer with hostname.
Use `ipconfig` in your terminal to find which ethernet adapter you want to monitor.
It may for example be `Wireless LAN adapter Wi-Fi`.
Then, monitor the IP address with `monitor_ip "wireless lan adapter wi-fi" "C:\Users\andre\Dropbox\my_computer_ip.txt"`, where the path in the end is there the IP address is saved.

## Save all .docx and .pptx files to PDFs (Requires Python)
This function `doc2pdf` saves all `.doc`, `.docx`, `.pptx` and `.pptm` files to `.pdf`.
It recursively looks through all the folders from the current folder.
To run, perform `doc2pdf` while being in the folder.

# Other Useful Programs
## Install software with Chocolatey
When I set up a new Windows 10 PC, I like to install most of my software using a script, rather than downloading 10-20-30-40 .exe files and executing them one by one manually.
This is achieved with [Chocolatey](https://chocolatey.org).
To install this on your system, follow the instructions [here](https://chocolatey.org/install).
**NB. This is not necessary to use the scripts in this repository.**

After installation, you can run, for example, `choco install autohotkey` in an elevated (admin) Powershell session.
Other programs I frequently use are located in `installation/installprograms.ps1`.

## Find files with Everything
If you ever struggle to find a certain file in Windows, it could be a pain to use the build-in search tools as it often does not find the files, or takes very long time to search.

To alleviate this, Everything from Voidtools makes this much easier by indexing beforehand (in a much better and faster way than Windows), and searches instantly.

Download and install Everything from [here](https://www.voidtools.com/downloads/).

Once the program is started, then it will initialize a scan of your entire drive. Once that is finished, searching is almost instant.

Try to search for `*.pdf` and you will see all files that ends with `.pdf`.
The wildcard * says that this can be matched with anything.
If you for example remember a presentation you had at some point that included the word "bearing" in the filename, but have no idea where it could be, you could for example search for `*bearing*.pptx`.

## Search text within a folder using grepWin
In for example a large codebase, you can use grepWin to search for certain word or phrase, and have it show you where this is located in any file, in any subfolder.

Download from [here](https://tools.stefankueng.com/grepWin.html) and install.

Once installed, you may right-click on any folder that you want to search in, and choose to `search with grepWin`.
Then, type in what to search for, and click `Search`.
To get a better view of the files or content, you may change that with the radio buttons in the lower right corner.

## Scan lines and get x-y data from plots with ScanIt
ScanIt from AmsterChem is useful for extracting certain values from x-y plots you may find in various datasheets or papers.

Just install it from [here](https://www.amsterchem.com/downloads.html) (should be at the bottom of the page), and open it.
Then, within ScanIt, you can paste an image from your clipboard (or import a png or jpg from file).
First you need to set the axis.
Use the numbers from the axes of the figure so that ScanIt can map the pixel positions to a certain x-y value.
Afterwards, you may use manual pick, or following lines.

Please see the tutorial [here](https://www.amsterchem.com/scanit.html)

## Combine PDFs into one with PdfTk
Download PdfTk from [here](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/).
After installation, just add PDFs, re-arrange if necessary, and Create PDF.

# Guides
## System variables (How-To)
We need to add and alter some system variables in order to run the scripts in here.

In order to access Environment variables:
1. Press the keys `WIN + R`
      1. A window named `run` should appear
2. Paste the following into that window: `SystemPropertiesAdvanced`
3. Click on `Environment variables` (eller `Miljøvariabler` på norsk)
4. We will focus on the `User variables` (up-most portion of the window)

To make a new variable:
1. Click on `New...`
2. Now you can name a variable, give it a value, and click `OK`.
3. Click `OK` in the `Variable Names` window in order to update the variables
4. To test if the variable worked OK, open a new CMD or Powershell session and type
   1. `echo %VARIABLE_NAME%
   2. where VARIABLE_NAME is the name of the variable that you made
   3. It should display the content of this variable

To edit PATH such that CMD or Powershell can find the scripts in this folder:
1. Find `Path` in the list under `User variables` and double click it
2. Click on `New` on the right, and add the desired path
3. These paths in here are checked when a script is run in CMD or Powershell, as Windows will only find scripts in these folders and the current folder.
