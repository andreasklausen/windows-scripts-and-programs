#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#SingleInstance force
#Include Gdip.ahk

^!n::
{
	EnvGet, dropbox, dropbox
	Run, "C:\Program Files\Microsoft VS Code\Code.exe" "%dropbox%\eternal\notes"
	return
}
^!s::
{
	EnvGet, userprofile, userprofile
	Run, "C:\Program Files\Microsoft VS Code\Code.exe" "%userprofile%\git\scripts"
	return
}
^!p::
{
	; https://www.autohotkey.com/boards/viewtopic.php?t=72431
	; https://www.autohotkey.com/boards/viewtopic.php?t=45019
	KeyWait, Ctrl
	KeyWait, Alt

	; Get the current URL or title of window
	if(WinActive("ahk_exe msedge.exe") || WinActive("ahk_exe firefox.exe") || WinActive("ahk_exe chrome.exe") || WinActive("ahk_exe brave.exe"))
	{
		Clipboard := ""
		Send ^l
		while !Clipboard
		{
			Sleep, 50
			Send ^c
		}
		Sleep, 50
		Send {Esc}
		title := Clipboard
	}
	else {
		WinGetTitle, title, A
	}
	; Set filename
	FileName := A_YYYY "-" A_MM "-" A_DD " " A_Hour "-" A_Min "-" A_Sec

	; Save the URL or title
	EnvGet, screenshot_folder, screenshot_folder
	FileAppend, %title%, %screenshot_folder%\%FileName%.txt ; screenshot_folder "\" FileName ".txt"

	; Make screen shot
	Send {Shift down}{LWin down}{s down}
	Send {Shift up}{LWin up}{s up}
	KeyWait, LButton, D
	KeyWait, LButton, L
	sleep, 200

	; Save clipboard image to file
	; https://www.autohotkey.com/boards/viewtopic.php?t=63872
	; https://www.autohotkey.com/boards/viewtopic.php?t=75637
	token := Gdip_Startup()
	cr:=Gdip_Savebitmaptofile(Gdip_CreateBitmapFromClipboard(), screenshot_folder "\" FileName ".png")
	Gdip_Shutdown(token)
	return
}

^!m::  Winset, Alwaysontop, Toggle, A
